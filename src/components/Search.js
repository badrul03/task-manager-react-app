const Search = () => {
  return (
    <div className="search-input text-right">
      <div className="form-group">
        <input type="text" name="search" placeholder="Search something..." />
      </div>
    </div>
  );
};

export default Search;
