import React from "react";
import ReactDOM from "react-dom";
import RouterComponent from "./routes";

ReactDOM.render(
  <>
    <RouterComponent />
  </>,
  document.getElementById("root")
);
