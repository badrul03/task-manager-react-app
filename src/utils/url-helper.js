export const AUTH_KEY = "token";

export const BASE_URL = "http://localhost:7070";

export const SIGNUP_URL = `${BASE_URL}/api/v1/auth/signup`;

export const LOGIN_URL = `${BASE_URL}/api/v1/auth/login`;
